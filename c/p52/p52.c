#define _POSIX_C_SOURCE 1
#include "../common.c"
#include <stdio.h>
#include <string.h>

int main()
{
    long x = 0;
    long x_digits[10];
    long nx_digits[10];
    while (1) {
        x++;
        count_digits(x_digits, x);
        int match = 1;
        for (int i = 2; i <= 6; i++) {
            count_digits(nx_digits, i*x);
            if (memcmp(x_digits, nx_digits, sizeof(x_digits)) != 0) {
                match = 0;
                break;
            }
        }
        if (match)
            break;
    }
    printf("%ld\n", x);
    return 0;
}
