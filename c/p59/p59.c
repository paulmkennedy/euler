#define _POSIX_C_SOURCE 1
#include "../common.c"
#include <ctype.h>

char msg[4096];
int msg_len = 0;
char decrypted[4096];
char buf[4096];

static void
decrypt(char * key, int key_len)
{
    for (int i = 0; i < msg_len; i++)
        decrypted[i] = msg[i] ^ key[i % key_len];
}

int main()
{
    FILE * fp = fopen("cipher.txt", "rb");
    fgets(buf, sizeof(buf), fp);
    char * saveptr = NULL;
    char * p = buf;
    char * delim = ",";
    char * token;
    while ((token = strtok_r(p, delim, &saveptr))) {
        p = NULL;
        long x = strtol(token, NULL, 10);
        assert(x >= 0);
        assert(x < 128);
        msg[msg_len++] = x;
    }

    char key[3];
    for (key[0] = 'a'; key[0] <= 'z'; key[0]++) {
        for (key[1] = 'a'; key[1] <= 'z'; key[1]++) {
            for (key[2] = 'a'; key[2] <= 'z'; key[2]++) {
                decrypt(key, 3);
                int i;
                for (i = 0; i < msg_len; i++) {
                    if (!isprint(decrypted[i]))
                        break;
                }
                //if (i == msg_len)
                //    printf("%s: %s\n", key, decrypted);
            }
        }
    }

    // from visual inspection, "exp" was determined to be the key
    decrypt("exp", 3);
    int sum = 0;
    for (int i = 0; i < msg_len; i++)
        sum += decrypted[i];
    printf("%d\n", sum);

    return 0;
}
