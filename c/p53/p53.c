#define _POSIX_C_SOURCE 1
#include "../common.c"

int main()
{
    long result = 0;
    for (int i = 1; i <= 100; i++) {
        int j = 1;
        for (; j <= i/2; j++) {
            long x = n_choose_k(i, j);
            if (x > 1e6)
                break;
        }
        long delta = (i/2 - (j-1))*2;
        if (delta > 0 && i % 2 == 0)
            delta -= 1;
        result += delta;
    }
    printf("%ld\n", result);
    return 0;
}
