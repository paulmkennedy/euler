#define _POSIX_C_SOURCE 1
#include "../common.c"
#include <limits.h>

u64
concat(u64 x, u64 y)
{
    u64 z = 1;
    u64 _y = y;
    while (_y > 0) {
        _y /= 10;
        z *= 10;
    }
    return x*z + y;
}

int
all_pairwise_concats_are_prime(u64 * p, size_t n)
{
    for (size_t i = 0; i < n; i++) {
        for (size_t j = 0; j < n; j++) {
            if (i == j)
                continue;
            u64 x = concat(p[i], p[j]);
            if (!is_prime(x))
                return 0;
        }
    }
    return 1;
}

int main()
{
    sieve(100000000);

    u64 p[5];
    u64 max_pi = 1100;
    assert(max_pi < primes_len);

    int answer = INT_MAX;
    for (u64 pi = 0; pi < max_pi; pi++) {
        //printf("%lu...\n", pi);
        p[0] = primes[pi];
        for (u64 pj = pi+1; pj < max_pi; pj++) {
            p[1] = primes[pj];
            if (!all_pairwise_concats_are_prime(p, 2))
                continue;
            for (u64 pk = pj+1; pk < max_pi; pk++) {
                p[2] = primes[pk];
                if (!all_pairwise_concats_are_prime(p, 3))
                    continue;
                for (u64 pl = pk+1; pl < max_pi; pl++) {
                    p[3] = primes[pl];
                    if (!all_pairwise_concats_are_prime(p, 4))
                        continue;
                    for (u64 pm = pl+1; pm < max_pi; pm++) {
                        p[4] = primes[pm];
                        if (!all_pairwise_concats_are_prime(p, 5))
                            continue;
                        // found solution:
                        int sum = 0;
                        for (size_t i = 0; i < NELEMS(p); i++) {
                            sum += p[i];
                            //printf("%zu ", p[i]);
                        }
                        if (sum < answer)
                            answer = sum;
                        //goto done;
                    }
                }
            }
        }
    }
//done:
    printf("%d\n", answer);
    return 0;
    // 26033
}
