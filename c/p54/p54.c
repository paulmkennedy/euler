#define _POSIX_C_SOURCE 1
#include "../common.c"
#include <stdlib.h>

#define NELEMS(X) (sizeof(X)/sizeof((X)[0]))

enum { JACK = 11, QUEEN, KING, ACE };

enum {
    RANK_HIGH, 
    RANK_ONE_PAIR, 
    RANK_TWO_PAIRS, 
    RANK_THREE_OF_A_KIND, 
    RANK_STRAIGHT, 
    RANK_FLUSH, 
    RANK_FULL_HOUSE, 
    RANK_FOUR_OF_A_KIND, 
    RANK_STRAIGHT_FLUSH, 
    RANK_ROYAL_FLUSH,
};

static int
card_name_to_int(const char * name)
{
    char rank_char = name[0];
    char suit_char = name[1];
    int rank;
    int suit;
    switch (rank_char) {
        case 'T': rank = 10;    break;
        case 'J': rank = JACK;  break;
        case 'Q': rank = QUEEN; break;
        case 'K': rank = KING;  break;
        case 'A': rank = ACE;   break;
        default: rank = rank_char - '0'; break;
    }
    switch (suit_char) {
        case 'H': suit = 0; break;
        case 'C': suit = 1; break;
        case 'S': suit = 2; break;
        case 'D': suit = 3; break;
    }
    return rank*4 + suit;
}

static int
suit(int card)
{
    return card % 4;
}

// NOTE: aces are always treated as high
static int
rank(int card)
{
    return card / 4;
}

static void
count_ranks(int ranks[15], int hand[5])
{
    memset(ranks, 0, 15*sizeof(ranks[0]));
    for (int i = 0; i < 5; i++) {
        ranks[rank(hand[i])]++;
    }
}

static int
rank_of_nth_highest_card(int hand[], int n)
{
    int ranks[5];
    for (int i = 0; i < 5; i++)
        ranks[i] = rank(hand[i]);
    qsort(ranks, 5, sizeof(int), cmpint);
    return ranks[5-n];
}

static int
high_card(int hand[])
{
    int result = 0;
    for (int i = 0; i < 5; i++) {
        if (rank(hand[i]) > rank(result))
            result = hand[i];
    }
    return result;
}

static int
one_pair(int hand[])
{
    int npairs = 0;
    int result = 0;
    int ranks[15];
    count_ranks(ranks, hand);
    for (int i = 2; i < 15; i++) {
        if (ranks[i] == 2) {
            npairs++;
            result = i;
        }
        if (ranks[i] > 2)
            return 0;
    }
    if (npairs == 1)
        return result;
    return 0;
}

static int
two_pairs(int hand[])
{
    int npairs = 0;
    int result = 0;
    int ranks[15];
    count_ranks(ranks, hand);
    for (int i = 2; i < 15; i++) {
        if (ranks[i] == 2) {
            npairs++;
            result = i;
        }
        if (ranks[i] > 2)
            return 0;
    }
    if (npairs == 2)
        return result;
    return 0;
}

static int
three_of_a_kind(int hand[])
{
    int result = 0;
    int ranks[15];
    count_ranks(ranks, hand);
    for (int i = 2; i < 15; i++) {
        if (ranks[i] > 1 && ranks[i] != 3)
            return 0;
        if (ranks[i] == 3)
            result = i;
    }
    return result;
}

static int
straight(int hand[])
{
    int ranks[5];
    for (int i = 0; i < 5; i++)
        ranks[i] = rank(hand[i]);
    qsort(ranks, 5, sizeof(int), cmpint);
    for (int i = 0; i < 4; i++)
        if (ranks[i+1] != ranks[i]+1)
            return 0;
    return rank(high_card(hand));
}

static int
flush(int hand[])
{
    int suit0 = suit(hand[0]);
    for (int i = 1; i < 5; i++) {
        if (suit(hand[i]) != suit0)
            return 0;
    }
    return 1;
}

static int
full_house(int hand[])
{
    int has_pair = 0, has_three = 0;
    int result = 0;
    int ranks[15];
    count_ranks(ranks, hand);
    for (int i = 2; i < 15; i++) {
        if (ranks[i] == 3) {
            result = i;
            has_three = 1;
        }
        if (ranks[i] == 2)
            has_pair = 1;
    }
    if (has_three && has_pair)
        return result;
    return 0;
}

static int
four_of_a_kind(int hand[])
{
    int ranks[15];
    count_ranks(ranks, hand);
    for (int i = 2; i < 15; i++) {
        if (ranks[i] == 4)
            return i;
    }
    return 0;
}

static int
straight_flush(int hand[])
{
    if (straight(hand) && flush(hand))
        return straight(hand);
    return 0;
}

#if 0
// NOTE: will always be straight flush
static int
royal_flush(int hand[])
{
    return straight_flush(hand) && rank(high_card(hand)) == ACE;
}
#endif

static void
parse_file(int ** hands, int * num_hands, int * hands_cap, const char * filename)
{
    char * saveptr;
    char * p;
    char * tok;
    FILE * fp = fopen(filename, "rb");
    if (fp == NULL) {
        perror("error");
        exit(1);
    }
    char buf[64];
    while (fgets(buf, sizeof(buf), fp) != NULL) {
        buf[strlen(buf)-1] = '\0'; // remove newline
        p = buf;
        int hi = 0;
        if (*num_hands >= *hands_cap) {
            *hands_cap = MAX(10, (*num_hands+1)*2);
            *hands = realloc(*hands, sizeof(**hands)*(*hands_cap)*10);
        }
        while ((tok = strtok_r(p, " ", &saveptr)) != NULL) {
            p = NULL;
            (*hands)[*num_hands*10 + hi] = card_name_to_int(tok);
            hi++;
        }
        (*num_hands)++;
    }
    fclose(fp);
}

int (*rank_functions[])(int *) = {
    [0] = high_card,
    [1] = one_pair,
    [2] = two_pairs,
    [3] = three_of_a_kind,
    [4] = straight,
    [5] = flush,
    [6] = full_house,
    [7] = four_of_a_kind,
    [8] = straight_flush,
//  [9] = royal_flush,
};

static int
hand_rank(int hand[5])
{
    for (int i = NELEMS(rank_functions)-1; i > 0; i--) {
        if (rank_functions[i](hand))
            return i;
    }
    return 0;
}

static int
choose_winner(int * hand0, int * hand1)
{
    int h0r = hand_rank(hand0);
    int h1r = hand_rank(hand1);
    if (h0r != h1r)
        return (h1r > h0r);

    int (*rank_func)(int *) = rank_functions[h0r];
    h0r = rank_func(hand0);
    h1r = rank_func(hand1);
    if (h0r != h1r)
        return (h1r > h0r);

    for (int i = 1; i <= 5; i++) {
        h0r = rank_of_nth_highest_card(hand0, i);
        h1r = rank_of_nth_highest_card(hand1, i);
        if (h0r != h1r)
            return (h1r > h0r);
    }

    return -1;
}

int main()
{
    int hands_cap = 0;
    int num_hands = 0;
    int * hands = NULL;
    parse_file(&hands, &num_hands, &hands_cap,  "poker.txt");

    int answer = 0;
    for (int i = 0; i < num_hands; i++) {
        int * hand0 = hands+i*10;
        int * hand1 = hands+i*10+5;
        int winner = choose_winner(hand0, hand1);
        if (winner == 0)
            answer++;
#if 0
        printf("hand %d wins: ", winner+1);
        for (int j = 0; j < 10; j++) {
            printf("%d ", hands[i*10 + j]);
            if (j == 4)
                printf("; ");
        }
        printf("\n");
#endif
    }

    printf("%d\n", answer);

    return 0;
}
