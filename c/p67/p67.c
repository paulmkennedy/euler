#define _POSIX_C_SOURCE 1
#include "../common.c"

long array[100][100];
long path_sums[100][100];

void
parse(char * filename)
{
    FILE * fp = fopen(filename, "r");
    if (fp == NULL) {
        perror(filename);
        exit(1);
    }
    char buf[1024];
    int r = 0;
    while (fgets(buf, sizeof(buf), fp)) {
        char * p = buf;
        char * saveptr = NULL;
        char * tok;
        int c = 0;
        while ((tok = strtok_r(p, " ", &saveptr))) {
            p = NULL;
            long x = strtol(tok, NULL, 10);
            array[r][c] = x;
            c++;
        }
        r++;
    }
    fclose(fp);
}

int
solve()
{
    for (int c = 0; c < 100; c++)
        path_sums[99][c] = array[99][c];
    for (int r = 98; r >= 0; r--) {
        for (int c = 0; c < r+1; c++) {
            path_sums[r][c] = array[r][c] + MAX(path_sums[r+1][c], path_sums[r+1][c+1]);
        }
    }
    return path_sums[0][0];
}

int main()
{
    parse("triangle.txt");
    int answer = solve();
    printf("%d\n", answer);
    return 0;
}
