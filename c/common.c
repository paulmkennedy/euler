#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <assert.h>

#define NELEMS(X) (sizeof(X)/sizeof((X)[0]))

#define MIN(A, B) ((A) < (B) ? (A) : (B))
#define MAX(A, B) ((A) > (B) ? (A) : (B))

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t   s8;
typedef int16_t  s16;
typedef int32_t  s32;
typedef int64_t  s64;
typedef float    f32;
typedef double   f64;

#if 0
typedef struct {
    char * data;
    size_t len;
    size_t cap;
} Buffer;

Buffer
read_file(const char * filename)
{
    FILE * fp = fopen(filename, "r");
    if (fp == NULL) {
        perror(filename);
        exit(EXIT_FAILURE);
    }
    struct stat sb;
    if (fstat(fileno(fp), &sb) == -1) {
        perror(filename);
        exit(EXIT_FAILURE);
    }
    Buffer buffer;
    buffer.len = (size_t) (sb.st_size+1);
    buffer.cap = buffer.len + 1;
    buffer.data = malloc(sizeof(*buffer.data)*buffer.cap);
    fread(buffer.data, 1, buffer.len, fp);
    buffer.data[buffer.len] = '\0';
    if (ferror(fp)) {
        perror(filename);
        exit(EXIT_FAILURE);
    }
    fclose(fp);
    return buffer;
}
#endif

s64
gcd(s64 x, s64 y)
{
    if (x == 0)
        return y;
    if (y == 0)
        return x;
    while (1) {
        s64 t = x;
        x = MIN(x, y);
        y = MAX(t, y);
        y %= x;
        if (y == 0)
            break;
        x -= y;
    }
    return x;
}

s64
lcm(s64 x, s64 y)
{
    return y*(x/gcd(x, y));
}

void
count_digits(long y[], long x)
{
    char buf[32];
    if (x < 0)
        x *= -1;
    memset(y, 0, sizeof(y[0])*10);
    sprintf(buf, "%ld", x);
    size_t len = strlen(buf);
    for (size_t i = 0; i < len; i++) {
        y[buf[i]-'0']++;
    }
}

long
factorial(long n)
{
    long result = 1;
    for (long i = 2; i <= n; i++) {
        result *= i;
    }
    return result;
}

long
n_choose_k(long n, long k)
{
    long result = 1;
    int ki = 2;
    for (int i = n; i > n-k; i--) {
        result *= i;
        if (ki <= k && result % ki == 0)
            result /= ki++;
    }
    return result;
}

int
cmpint(const void * a, const void * b)
{
    return *(int *) a - *(int *) b;
}

int
cmpchar(const void * a, const void * b)
{
    char ai = *((char *) a);
    char bi = *((char *) b);
    return ai - bi;
}

void
reverse(char * s)
{
    int len = strlen(s);
    for (int i = 0; i < len/2; i++) {
        char t = s[i];
        s[i] = s[len-i-1];
        s[len-i-1] = t;
    }
}

int
is_palindrome(const char * s)
{
    int len = strlen(s);
    for (int i = 0; i < len/2; i++) {
        if (s[i] != s[len-i-1])
                return 0;
    }
    return 1;
}

typedef struct {
    u8 * digits;
    int ndigits;
    int digits_cap;
    int negative;
} BigInt;

BigInt
bigint_from_s64(s64 x)
{
    BigInt y = {0};
    if (x == 0)
        return y;
    if (x < 0) {
        x *= -1;
        y.negative = 1;
    }
    s64 t = x;
    while (t > 0) {
        y.ndigits++;
        t /= 10;
    }
    y.digits = malloc(sizeof(*y.digits)*y.ndigits);
    t = x;
    for (int i = 0; i < y.ndigits; i++, t /= 10) {
        y.digits[i] = t % 10;
    }
    return y;
}

void
bigint_print(BigInt x, FILE * fp)
{
    int n = x.ndigits;
    if (n == 0) {
        fprintf(fp, "0");
        return;
    }
    if (x.negative)
        printf("-");
    for (int i = 0; i < n; i++) {
        fprintf(fp, "%d", x.digits[n-i-1]);
    }
}

BigInt
bigint_mul(BigInt x, BigInt y)
{
    BigInt z = {0};
    z.digits = calloc(x.ndigits + y.ndigits, sizeof(*z.digits));
    if (x.negative ^ y.negative)
        z.negative = 1;
    for (int i = 0; i < y.ndigits; i++) {
        for (int j = 0; j < x.ndigits; j++) {
            int a = x.digits[j] * y.digits[i];
            z.digits[i+j] += a;
        }
    }
    for (int i = 0; i < x.ndigits + y.ndigits - 1; i++) {
        z.digits[i+1] += z.digits[i] / 10;
        z.digits[i] %= 10;
    }
    for (int i = x.ndigits + y.ndigits - 1; i >= 0; i--) {
        if (z.digits[i]) {
            z.ndigits = i + 1;
            break;
        }
    }
    return z;
}

typedef struct {
    s64 numer;
    s64 denom;
} Fractional;

Fractional
fractional_add(Fractional frac1, Fractional frac2)
{
    s64 t = lcm(frac1.denom, frac2.denom);
    frac1.numer *= t/frac1.denom;
    frac2.numer *= t/frac2.denom;
    frac1.numer += frac2.numer;
    frac1.denom = t;
    return frac1;
}

Fractional
fractional_inv(Fractional frac)
{
    assert(frac.denom != 0);
    s64 t = frac.numer;
    frac.numer = frac.denom;
    frac.denom = t;
    return frac;
}

Fractional
fractional_reduce(Fractional frac)
{
    s64 t = gcd(frac.numer, frac.denom);
    frac.numer /= t;
    frac.denom /= t;
    return frac;
}

u64
up_to_pow2(u64 n)
{
    u64 result = 1;
    if (n <= 0)
        return 0;
    n--;
    while (n) {
        n >>= 1;
        result *= 2;
    }
    return result;
}

u64 * primes;
u64 primes_cap;
u64 primes_len;

// TODO: this could probably be simplified
bool
sieve(u64 n)
{
    enum {UNKNOWN, COMPOSITE, PRIME};
    static char * array;
    static u64 cap;
    if (n < cap)
        return array[n] == PRIME;
    u64 new_cap = up_to_pow2(n+1);
    if (new_cap > cap) {
        array = realloc(array, sizeof(*array)*new_cap);
        memset(array+cap, 0, new_cap - cap);
        cap = new_cap;
    }
    memset(array, 0, cap); // TODO: see if this can be removed
    array[2] = PRIME;
    for (u64 i = 4; i <= cap; i += 2)
        array[i] = COMPOSITE;
    if (primes_cap == 0) {
        primes_cap = 1024;
        primes = malloc(sizeof(*primes)*primes_cap);
        primes[0] = 2;
    }
    u64 p = 3;
    primes_len = 1;
    while (1) {
        array[p] = PRIME;
        if (primes_len >= primes_cap) {
            primes_cap *= 2;
            primes = realloc(primes, sizeof(*primes)*primes_cap);
        }
        primes[primes_len++] = p;
        for (u64 i = p*p; i <= cap; i += 2*p) {
            array[i] = COMPOSITE;
        }
        for (p++; p < cap && array[p]; p++)
            ;
        if (p >= cap)
            break;
    }
    return array[n] == PRIME;
}

bool
is_prime(u64 x)
{
    return sieve(x);
}

#if 0
u64
nth_prime(u64 n)
{
    // TODO: choose a good m given n
    // want inverse of prime-counting function
    // probably need to use an iterative approach
    u64 m = 100;
    sieve(m);
    while (primes_len < n) {
        m *= 2;
        sieve(m);
    }
    return primes[n];
}
#endif
