#define _POSIX_C_SOURCE 1
#include "../common.c"
#include <math.h>

int
is_triangle(u64 x)
{
    u64 n = round(sqrt(1/4 + 2*x) - 1.0/2);
    return (n*(n+1)/2 == x);
}

int
is_square(u64 x)
{
    u64 n = sqrt(x);
    return n*n == x;
}

int
is_pentagonal(u64 x)
{
    u64 n = round(sqrt(1.0/4 + 6*x)/3 + 1.0/6);
    return (n*(3*n-1)/2 == x);
}

int
is_hexagonal(u64 x)
{
    u64 n = round(sqrt(1 + 8*x)/4 + 1.0/4);
    return (n*(2*n-1) == x);
}

int
is_heptagonal(u64 x)
{
    u64 n = round(sqrt(9.0/4 + 10*x)/5 + (3.0/10));
    return (n*(5*n-3)/2 == x);
}

int
is_octagonal(u64 x)
{
    u64 n = round(sqrt(4 + 12*x)/6 + 2.0/6);
    return (n*(3*n-2) == x);
}

int (*polygonal_funcs[])(u64) = {
    is_triangle,
    is_square,
    is_pentagonal,
    is_hexagonal,
    is_heptagonal,
    is_octagonal,
};

int
is_identity_like(int * mat, int n)
{
    for (int r = 0; r < n; r++) {
        int sum = 0;
        for (int c = 0; c < n; c++)
            sum += mat[r*n + c];
        if (sum != 1)
            return 0;
    }
    for (int c = 0; c < n; c++) {
        int sum = 0;
        for (int r = 0; r < n; r++)
            sum += mat[r*n + c];
        if (sum != 1)
            return 0;
    }
    return 1;
}

int polygonal_numbers[6][100];
int pnlen[6];

void
find_polygonal_numbers()
{
    int counts[6] = {0};
    for (int i = 1010; i <= 9999; i++) {
        for (int j = 0; j < 6; j++) {
            if (polygonal_funcs[j](i)) {
                counts[j]++;
                polygonal_numbers[j][pnlen[j]++] = i;
            }
        }
    }
#if 0
    for (int j = 0; j < 6; j++) {
        printf("%d: %d\n", j+3, counts[j]);
        for (int k = 0; k < pnlen[j]; k++) {
            printf("  %d\n", polygonal_numbers[j][k]);
        }
    }
#endif
}

void
find_three_4digit_numbers()
{
    int i[3];
    int mat[3][3];
    for (i[0] = 1010; i[0] <= 9999; i[0]++) {
        if (i[0] % 100 < 10)
            continue;
        for (int fi = 0; fi < 3; fi++)
            mat[fi][0] = polygonal_funcs[fi](i[0]);
        int i1_0 = (i[0] % 100)*100 + 10;
        for (i[1] = i1_0; i[1] < i1_0 + 90; i[1]++) {
            if (i[1] < i[0])
                continue;
            i[2] = (i[1] % 100)*100 + i[0] / 100;
            for (int fi = 0; fi < 3; fi++) {
                mat[fi][1] = polygonal_funcs[fi](i[1]);
                mat[fi][2] = polygonal_funcs[fi](i[2]);
            }
            if (is_identity_like((int *) mat, 3))
                printf("%d %d %d\n", i[0], i[1], i[2]);
        }
    }
}

#if 0
void
find_six_4digit_numbers()
{
    int xxx = 0;
    int i[6];
    int mat[6][6];
    for (i[0] = 1010; i[0] <= 9999; i[0]++) {
        if (i[0] % 100 < 10)
            continue;
        for (int fi = 0; fi < 6; fi++)
            mat[fi][0] = polygonal_funcs[fi](i[0]);
        int i1_0 = (i[0] % 100)*100 + 10;
        for (i[1] = i1_0; i[1] < i1_0 + 90; i[1]++) {
            if (i[1] < i[0])
                continue;
            for (int fi = 0; fi < 6; fi++)
                mat[fi][1] = polygonal_funcs[fi](i[1]);
            int i2_0 = (i[1] % 100)*100 + 10;
            for (i[2] = i2_0; i[2] < i2_0 + 90; i[2]++) {
                if (i[2] < i[1])
                    continue;
                for (int fi = 0; fi < 6; fi++)
                    mat[fi][2] = polygonal_funcs[fi](i[2]);
                int i3_0 = (i[2] % 100)*100 + 10;
                for (i[3] = i3_0; i[3] < i3_0 + 90; i[3]++) {
                    if (i[3] < i[2])
                        continue;
                    for (int fi = 0; fi < 6; fi++)
                        mat[fi][3] = polygonal_funcs[fi](i[3]);
                    int i4_0 = (i[3] % 100)*100 + 10;
                    for (i[4] = i4_0; i[4] < i4_0 + 90; i[4]++) {
                        if (i[4] < i[3])
                            continue;
                        i[5] = (i[4] % 100)*100 + i[0] / 100;
                        for (int fi = 0; fi < 6; fi++) {
                            mat[fi][4] = polygonal_funcs[fi](i[4]);
                            mat[fi][5] = polygonal_funcs[fi](i[5]);
                        }
                        if (is_identity_like((int *) mat, 6)) {
                            for (int j = 0; j < 6; j++)
                                printf("%d ", i[j]);
                            printf("\n");
                            return ;
                        }
                        if (xxx++ % 1000000 == 0) {
                            fprintf(stderr, "%d\n", xxx/1000000);
                        }
                    }
                }
            }
        }
    }
}
#endif

static void
swap(void * x,  void * y, size_t size)
{
    char buffer[1024];
    void * t = buffer;
    if (size > sizeof(buffer))
        t = malloc(size);
    memcpy(t, x, size);
    memcpy(x, y, size);
    memcpy(y, t, size);
    if (t != buffer)
        free(t);
}

int
permute(void * base, size_t nmemb, size_t size, int (*compar)(const void *, const void *))
{
#define CMP(BASE, I, J) compar(BASE + size*(I), BASE + size*(J))
    char * xs = (char *) base;
    size_t i;
    for (i = 0; i < nmemb-1; i++)
        if (CMP(xs, i, i+1) < 0)
            break;
    if (i == nmemb-1)
        return 1;
    int k = -1;
    for (size_t j = 0; j < i+1; j++)
        if (CMP(xs, j, i+1) < 0 && (k < 0 || CMP(xs, j, k) > 0))
            k = j;
    assert(k >= 0);
    swap(xs + size*(i+1), xs + size*k, size);
    qsort(xs, i+1, size, compar);
    return 0;
#undef CMP
}

static void
print_list(int * xs, int n)
{
    for (int i = 0; i < n; i++) {
        printf("%d ", xs[i]);
    }
    printf("\n");
}

int main()
{
//    for (int i = 1; i <= 40; i++)
//        printf("%d is %sa octagonal number\n", i, is_octagonal(i) ? "" : "not ");
    find_polygonal_numbers();

    //find_six_4digit_numbers();
    
    int xs[] = {1, 2, 3, 4};
    do {
        print_list(xs, NELEMS(xs));
    } while (!permute(xs, NELEMS(xs), sizeof(xs[0]), cmpint));

    return 0;
}
