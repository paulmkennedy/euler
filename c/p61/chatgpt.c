#include <stdio.h>
#include <stdlib.h>

// Function to swap two elements of the list
void swap(void *a, void *b, size_t size) {
    unsigned char *p = a, *q = b, tmp;
    for (size_t i = 0; i < size; i++) {
        tmp = p[i];
        p[i] = q[i];
        q[i] = tmp;
    }
}

// Function to reverse a portion of the list
void reverse(void *base, size_t nmemb, size_t size) {
    unsigned char *start = base;
    unsigned char *end = start + (nmemb - 1) * size;
    while (start < end) {
        swap(start, end, size);
        start += size;
        end -= size;
    }
}

// Function to find the next permutation
int permute(void *base, size_t nmemb, size_t size, int (*compar)(const void *, const void *)) {
    unsigned char *list = base;

    // Find the largest index k such that list[k] < list[k+1]
    int k = -1;
    for (int i = nmemb - 2; i >= 0; i--) {
        if (compar(&list[i * size], &list[(i + 1) * size]) < 0) {
            k = i;
            break;
        }
    }

    // If such an index doesn't exist, the list is already in the final permutation
    if (k == -1) {
        return 1;
    }

    // Find the largest index l greater than k such that list[k] < list[l]
    int l = -1;
    for (int i = nmemb - 1; i > k; i--) {
        if (compar(&list[k * size], &list[i * size]) < 0) {
            l = i;
            break;
        }
    }

    // Swap list[k] and list[l]
    swap(&list[k * size], &list[l * size], size);

    // Reverse the sequence from list[k+1] up to and including the final element
    reverse(&list[(k + 1) * size], nmemb - k - 1, size);

    return 0;
}

// Comparison function for integers
int cmpint(const void *a, const void *b) {
    return (*(int *)a - *(int *)b);
}

// Function to print a list of integers
void print_list(int *list, size_t nmemb) {
    for (size_t i = 0; i < nmemb; i++) {
        printf("%d ", list[i]);
    }
    printf("\n");
}

int main() {
    int xs[] = {1, 2, 3};
    do {
        print_list(xs, 3);
    } while (!permute(xs, 3, sizeof(xs[0]), cmpint));
    return 0;
}
