#define _POSIX_C_SOURCE 1
#include "../common.c"

int main()
{
    u64 max = 0;
    for (int i = 2; i < 100; i++) {
        BigInt x = bigint_from_s64(1);
        BigInt ibi = bigint_from_s64(i);
        for (int j = 1; j < 100; j++) {
            x = bigint_mul(x, ibi); // XXX: memory leak
            u64 sum = 0;
            for (int i = 0; i < x.ndigits; i++) {
                sum += x.digits[i];
            }
            if (sum > max) {
                max = sum;
                //printf("new max: %d^%d == ", i, j);
                //bigint_print(x, stdout);
                //printf(" (digit sum: %ld)\n", sum);
            }
        }
    }
    printf("%lu\n", max);
    return 0;
}
