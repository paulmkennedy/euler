#define _POSIX_C_SOURCE 1
#include "../common.c"

int main()
{
    sieve(700000000);
    int nprimes = 0;
    int total = 1;
    u64 s;
    for (s = 3; ; s += 2) {
        u64 n = s - 1;
        u64 corners[4];
        corners[0] = n*n-n+1;
        corners[1] = n*n+1;
        corners[2] = n*n+n+1;
        corners[3] = n*n+2*n+1;
        total += 4;
        for (int i = 0; i < 4; i++)
            nprimes += is_prime(corners[i]);
        double percent = (1.0*nprimes)/total;
        if (percent < 0.10)
            break;
    }
    printf("%lu\n", s);
    return 0;
}
