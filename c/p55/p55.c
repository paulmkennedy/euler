#define _POSIX_C_SOURCE 1
#include "../common.c"
#include <stdlib.h>
#include <inttypes.h>

static int
is_lychrel(u64 x)
{
    char buf[64];
    for (int i = 0; i < 50; i++) {
        sprintf(buf, "%" PRId64, x);
        reverse(buf);
        u64 x_rev = strtol(buf, NULL, 10);
        x += x_rev;
        sprintf(buf, "%" PRId64, x);
        if (is_palindrome(buf))
            return 0;
    }
    return 1;
}

int main()
{
    int answer = 0;
    for (int i = 1; i < 10000; i++) {
        if (is_lychrel(i))
            answer++;
    }
    printf("%d\n", answer);
    return 0;
}
