#define _POSIX_C_SOURCE 1
#include "../common.c"

int main()
{
#if 0
    int answer = 0;
    Fractional frac_1 = { .numer = 1, .denom = 1 };
    Fractional frac_2 = { .numer = 2, .denom = 1 };
    Fractional frac_x = { .numer = 1, .denom = 2 };
    for (int i = 1; i < 50; i++) {
        Fractional frac_y = fractional_add(frac_x, frac_1);
        frac_y = fractional_reduce(frac_y);
        int nn = snprintf(NULL, 0, "%ld", frac_y.numer);
        int nd = snprintf(NULL, 0, "%ld", frac_y.denom);
        if (nn > nd) {
            answer++;
            printf("*");
        }
        printf("%d: %ld/%ld\n", i, frac_y.numer, frac_y.denom);

        frac_x = fractional_add(frac_2, frac_x);
        frac_x = fractional_inv(frac_x);
        frac_x = fractional_reduce(frac_x);
    }
    printf("%d\n", answer);
#endif

    // based on the pattern produced by the code above, it appears that the
    // answer can be calculated using the following:

    int count = 0;
    int z = 1;
    while (1) {
        z += 8;
        if (z > 1000)
            break;
        count++;
        z += 5;
        if (z > 1000)
            break;
        count++;
    }
    printf("%d\n", count);

    return 0;
}
